page {
    includeJSFooter {
        lightcaseJs = https://cdnjs.cloudflare.com/ajax/libs/lightcase/2.4.2/js/lightcase.min.js
        lightcaseJs.external = 1
        waypoints = https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js
        waypoints.external = 1
        mixitup = https://cdnjs.cloudflare.com/ajax/libs/mixitup/3.3.0/mixitup.min.js
        mixitup.external = 1
        baseJs = EXT:base/Resources/Public/Scripts/base.js
    }

    # CSS files to be included
    includeCSS {
        bootstrapFour >
        customBootstrap >
        boostrap = EXT:base/Resources/Public/Css/bootstrap.scss
        lightcaseCss = https://cdnjs.cloudflare.com/ajax/libs/lightcase/2.4.2/css/lightcase.min.css
        lightcaseCss.external = 1
        animateCss >
        animateCss = https://daneden.github.io/animate.css/animate.min.css
        animateCss.external = 1
        baseCss = EXT:base/Resources/Public/Css/base.css
        baseScss = EXT:base/Resources/Public/Css/base.scss
        cookieconsentCss >
        cookieconsentCss = https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css
        cookieconsentCss.excludeFromConcatenation = 1
        cookieconsentCss.disableCompression = 1
        cookieconsentCss.external = 1
    }

    includeJS {
        cookieconsentJs = https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js
        cookieconsentJs.excludeFromConcatenation = 1
        cookieconsentJs.disableCompression = 1
        cookieconsentJs.external = 1
    }
}


