page = PAGE
page {
   1 = LOAD_REGISTER
   1 {
      pageLayout.cObject = TEXT
      pageLayout.cObject {
         data = levelfield:-1, backend_layout_next_level, slide
         override.field = backend_layout
         split {
            token = pagets__
            1.current = 1
            1.wrap = |
         }
      }
   }
   10 = FLUIDTEMPLATE
   10 {
      templateName = TEXT
      templateName.data = register:pageLayout
      format = html
      templateRootPaths {
         10 = EXT:base/Resources/Private/Page/Templates/
      }
      partialRootPaths {
         10 = EXT:base/Resources/Private/Page/Partials/
      }
      layoutRootPaths {
         10 = EXT:base/Resources/Private/Page/Layouts/
      }
      variables {
         layout = TEXT
         layout.data = register:pageLayout
      }
   }
   meta.viewport  = width=device-width, initial-scale=1.0
}

lib.content0 < styles.content.get
lib.content1 < styles.content.getLeft
lib.content2 < styles.content.getRight
lib.content3 < styles.content.getBorder
lib.content4 < styles.content.get
lib.content4.select.where = colPos=4
lib.content5 < styles.content.get
lib.content5.select.where = colPos=5

lib.contentElement.templateRootPaths.10 = EXT:base/Resources/Private/CoreContent/Templates/
lib.contentElement.partialRootPaths.10 = EXT:base/Resources/Private/CoreContent/Partials/
lib.contentElement.layoutRootPaths.10 = EXT:base/Resources/Private/CoreContent/Layouts/

tt_content.image.dataProcessing.20.maxGalleryWidthInText = 2000
tt_content.image.dataProcessing.20.maxGalleryWidth = 2000
tt_content.textmedia.dataProcessing.20.maxGalleryWidthInText = 2000
tt_content.textmedia.dataProcessing.20.maxGalleryWidth = 2000

lib.gridelements.baseSetup < lib.gridelements.defaultGridSetup
lib.gridelements.baseSetup.cObject = FLUIDTEMPLATE
lib.gridelements.baseSetup.cObject {
   templateName = Default
   format = html
   templateRootPaths {
      10 = EXT:base/Resources/Private/Grid/Templates
   }
   partialRootPaths {
      10 = EXT:base/Resources/Private/Grid/Partials
   }
   layoutRootPaths {
      10 = EXT:base/Resources/Private/Grid/Layouts
   }
}

config.tx_realurl_enable = 1


lib.contentElement {
    dataProcessing {
        321 = TYPO3\CMS\Frontend\DataProcessing\DatabaseQueryProcessor
        321 {
            if.isTrue.field = categories
            table = sys_category
            selectFields = sys_category.*
            pidInList = 1
            recursive = 999
            join = sys_category_record_mm ON sys_category.uid = sys_category_record_mm.uid_local
            where.data = field:uid
            where.wrap = sys_category_record_mm.tablenames = 'tt_content' and sys_category_record_mm.uid_foreign='|'
            as = data_categories
        }
    }
}

tt_content.base_slider =< lib.contentElement
tt_content.base_slider {
    layoutRootPaths.0 = EXT:base/Resources/Private/Content/Layouts/
    layoutRootPaths.30 = EXT:base/Resources/Private/CoreContent/Layouts/
    partialRootPaths.0 = EXT:base/Resources/Private/Content/Partials/
    templateRootPaths.0 = EXT:base/Resources/Private/Content/Templates/
    templateName = Slider
    dataProcessing.10 = TYPO3\CMS\Frontend\DataProcessing\DatabaseQueryProcessor
    dataProcessing.10 {
        if.isTrue.field = tx_base_slider_slides
        table = tx_base_slider_slides
        pidInList.field = pid
        where = parentid=###uid### AND deleted=0 AND hidden=0
        orderBy = sorting
        markers {
            uid.field = uid
        }
        as = data_tx_base_slider_slides
    }
dataProcessing.10 {
        dataProcessing.10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
    dataProcessing.10 {
        if.isTrue.field = tx_base_slider_picture
        references {
            fieldName = tx_base_slider_picture
            table = tx_base_slider_slides
        }
        as = data_tx_base_slider_picture
    }

}
}


tt_content.base_quote =< lib.contentElement
tt_content.base_quote {
    layoutRootPaths.0 = EXT:base/Resources/Private/Content/Layouts/
    layoutRootPaths.30 = EXT:base/Resources/Private/CoreContent/Layouts/
    partialRootPaths.0 = EXT:base/Resources/Private/Content/Partials/
    templateRootPaths.0 = EXT:base/Resources/Private/Content/Templates/
    templateName = Quote
    dataProcessing.10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
    dataProcessing.10 {
        if.isTrue.field = tx_base_quote_picture
        references {
            fieldName = tx_base_quote_picture
            table = tt_content
        }
        as = data_tx_base_quote_picture
    }
}

tt_content.base_contentfilter =< lib.contentElement
tt_content.base_contentfilter {
    layoutRootPaths.0 = EXT:base/Resources/Private/Content/Layouts/
    layoutRootPaths.30 = EXT:base/Resources/Private/CoreContent/Layouts/
    partialRootPaths.0 = EXT:base/Resources/Private/Content/Partials/
    templateRootPaths.0 = EXT:base/Resources/Private/Content/Templates/
    templateName = ContentFilter
}

# Fix cs_seo bug
page.headerData.654.7.data = page:description
