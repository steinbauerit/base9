<?php
	$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][4] = [
        'Bilder',
        'image',
        'content-image'
    ];
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][5] = [
        'Medien',
        'textmedia',
        'content-textmedia'
    ];

    $GLOBALS['TCA']['tt_content']['types']['textmedia']['columnsOverrides'] = [
        'image_zoom' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'bodytext' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'tx_base_gallery_type' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ]
    ];

    $GLOBALS['TCA']['tt_content']['types']['t3sbs_card']['columnsOverrides'] = [
        'tx_t3sbootstrap_extra_class' => [
            'label' => 'Extra Class (wenn Image overlay aktiviert: "hover" für Overlay on hover einblenden, "hover zoom" für zusätzlich zoom on hover)'
        ],
    ];

    $GLOBALS['TCA']['tt_content']['columns']['tx_t3sbootstrap_container']['config']['items'] = [
        ['Standard',''],
        ['Inhaltsbreite','container'],
        ['Volle Breite','container-fluid']
    ];

    $GLOBALS['TCA']['tt_content']['columns']['tx_t3sbootstrap_contextcolor']['config']['items'] = [
        ['Keine',''],
        ['Weiß', 'white'],
        ['Hauptfarbe','primary'],
        ['Zweitfarbe','secondary'],
        ['Erfolg','success'],
        ['Info','info'],
        ['Warnung','warning'],
        ['Achtung','danger'],
        ['Spezial 1','customOne'],
        ['Spezial 2','customTwo']
    ];

    $GLOBALS['TCA']['tt_content']['columns']['tx_t3sbootstrap_textcolor']['config']['items'] = [
        ['Keine',''],
        ['Weiß', 'white'],
        ['Hell','muted'],
        ['Hauptfarbe','primary'],
        ['Zweitfarbe','secondary'],
        ['Erfolg','success'],
        ['Info','info'],
        ['Warnung','warning'],
        ['Achtung','danger'],
        ['Spezial 1','custom1'],
        ['Spezial 2','custom2']
    ];

    

    $GLOBALS['TCA']['tt_content']['types']['gridelements_pi1']['columnsOverrides'] = [
        'tx_t3sbootstrap_header_class' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'header_layout' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'header_position' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'header_link' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'media' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ]
    ];


    # add palette bootstrap
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tt_content',
        '--palette--;Bootstrap Color;bootstrapColor',
        '',
        'after:layout'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tt_content',
        '--palette--;Bootstrap Utilities;bootstrap',
        '',
        'after:layout'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tt_content',
        '--palette--;Bootstrap Spacing;bootstrapSpacing',
        '',
        'after:layout'
    );

    
    


    $galleryType = array(
        'tx_base_gallery_type' => array (
            'exclude' => 0,
            'label' => 'Klick-Vergrößern Galerie-Typ',
            'config' => array (
                'type' => 'select',
                'items' => array (
                        array('Normal', '0'),
                        array('Seitenweite Galerie', '1'),
                        array('Keine Galerie', '2'),
                ),
                'size' => 1,
                'maxitems' => 1,
        	)
        )
	);
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'tt_content',
        $galleryType
	);
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'imagelinks',
        'tx_base_gallery_type',
        'after:image_zoom'
	);


  $enableFilter = array(
        'tx_base_enable_filter' => array (
            'exclude' => 0,
            'label' => 'Filter aktivieren (für Inhalts-Filter; mind. 1 Kategorie muss gewählt sein)',
            'config' => array (
                'type' => 'check',
                'default' => '0'
            )
        )
  );
  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'tt_content',
        $enableFilter
  );
  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'filter',
        'tx_base_enable_filter',
        ''
  );

  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tt_content',
        '--palette--;Filter;filter',
        '',
        'after:categories'
    );


    $animations = array(
        'tx_base_animation_type' => [
            'label' => 'Typ',
            'exclude' => 1,
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['Keine',''],
                    ['Attention Seekers', '--div--'],
                    ['bounce','bounce'],
                    ['flash','flash'],
                    ['pulse','pulse'],
                    ['rubberBand','rubberBand'],
                    ['shake','shake'],
                    ['headShake','headShake'],
                    ['swing','swing'],
                    ['tada','tada'],
                    ['wobble','wobble'],
                    ['jello','jello'],
                    ['Bouncing Entrances', '--div--'],
                    ['bounceIn','bounceIn'],
                    ['bounceInDown','bounceInDown'],
                    ['bounceInLeft','bounceInLeft'],
                    ['bounceInRight','bounceInRight'],
                    ['bounceInUp','bounceInUp'],
                    ['Fading Entrances', '--div--'],
                    ['fadeIn','fadeIn'],
                    ['fadeInDown','fadeInDown'],
                    ['fadeInDownBig','fadeInDownBig'],
                    ['fadeInLeft','fadeInLeft'],
                    ['fadeInLeftBig','fadeInLeftBig'],
                    ['fadeInRight','fadeInRight'],
                    ['fadeInRightBig','fadeInRightBig'],
                    ['fadeInUp','fadeInUp'],
                    ['fadeInUpBig','fadeInUpBig'],
                    ['Flipping Entrances', '--div--'],
                    ['flipInX','flipInX'],
                    ['flipInY','flipInY'],
                    ['Rotating Entrances', '--div--'],
                    ['rotateIn','rotateIn'],
                    ['rotateInDownLeft','rotateInDownLeft'],
                    ['rotateInDownRight','rotateInDownRight'],
                    ['rotateInUpLeft','rotateInUpLeft'],
                    ['rotateInUpRight','rotateInUpRight'],
                    ['Special Entrances', '--div--'],
                    ['jackInTheBox','jackInTheBox'],
                    ['lightSpeedIn','lightSpeedIn'],
                    ['rollIn','rollIn'],
                    ['Zooming Entrances', '--div--'],
                    ['zoomIn','zoomIn'],
                    ['zoomInDown','zoomInDown'],
                    ['zoomInLeft','zoomInLeft'],
                    ['zoomInRight','zoomInRight'],
                    ['zoomInUp','zoomInUp'],
                    ['Sliding Entrances', '--div--'],
                    ['slideInDown','slideInDown'],
                    ['slideInLeft','slideInLeft'],
                    ['slideInRight','slideInRight'],
                    ['slideInUp','slideInUp'],
                ],
                'default' => ''
            ]
        ],
        'tx_base_animation_delay' => [
            'exclude' => 1,
            'label'  => 'Verzögerung (mit Einheit, ms oder s)',
            'config' => [
                'type' => 'input',
                'size' => 35,
                'default' => '',
                'placeholder' => '500ms'
            ]
        ],
        'tx_base_no_gutters' => [
            'exclude' => 1,
            'label'  => 'Keine Spaltenabstände',
            'config' => [
              'type' => 'check',
              'default' => '0',
            ]
        ],
        'tx_base_align' => [
            'exclude' => 1,
            'label'  => 'Ausrichtung',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['Standard',''],
                    ['Links', 'text-left'],
                    ['Mitte','text-center'],
                    ['Rechts','text-right']
                ],
                'default' => ''
            ]
        ],
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'tt_content',
        $animations
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'animation',
        'tx_base_animation_type',
        ''
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'animation',
        'tx_base_animation_delay',
        'after:tx_base_animation_type'
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tt_content',
        '--palette--;Animationen;animation',
        '',
        'after:layout'
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'gallerySettings',
        'tx_base_no_gutters',
        'after:imagecols'
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'bootstrap',
        'tx_base_align',
        'before:tx_t3sbootstrap_extra_class'
    );

    $GLOBALS['TCA']['tt_content']['palettes']['animation'] = [
      'showitem' => 'tx_base_animation_type, tx_base_animation_delay'
    ];


    $tempColumns = array (
      'tx_base_slider_controls' => 
      array (
        'config' => 
        array (
          'type' => 'check',
          'default' => '1',
        ),
        'exclude' => '1',
        'label' => 'LLL:EXT:base/Resources/Private/Language/locallang_db.xlf:tt_content.tx_base_slider_controls',
      ),
      'tx_base_slider_indicators' => 
      array (
        'config' => 
        array (
          'type' => 'check',
          'default' => '1',
        ),
        'exclude' => '1',
        'label' => 'LLL:EXT:base/Resources/Private/Language/locallang_db.xlf:tt_content.tx_base_slider_indicators',
      ),
      'tx_base_slider_use_fade' => 
      array (
        'config' => 
        array (
          'type' => 'check',
          'default' => '0',
        ),
        'exclude' => '1',
        'label' => 'Fading statt Sliding-Übergang',
      ),
      'tx_base_slider_slides' => 
      array (
        'config' => 
        array (
          'type' => 'inline',
          'foreign_table' => 'tx_base_slider_slides',
          'foreign_field' => 'parentid',
          'foreign_table_field' => 'parenttable',
          'foreign_sortby' => 'sorting',
          'appearance' => 
          array (
            'enabledControls' => 
            array (
              'dragdrop' => '1',
            ),
            'newRecordLinkTitle' => 'Neuer Slide',
            'levelLinksPosition' => 'top',
            'useSortable' => '1',
          ),
          'minitems' => '1',
        ),
        'exclude' => '1',
        'l10n_mode' => 'copy',
        'label' => 'LLL:EXT:base/Resources/Private/Language/locallang_db.xlf:tt_content.tx_base_slider_slides',
      ),
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array(
        'LLL:EXT:base/Resources/Private/Language/locallang_db.xlf:tt_content.CType.div._base_',
        '--div--',
    );
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array(
        'LLL:EXT:base/Resources/Private/Language/locallang_db.xlf:tt_content.CType.base_slider',
        'base_slider',
        'tx_base_slider',
    );
    $tempTypes = array (
      'base_slider' => 
      array (
        'columnsOverrides' => 
        array (
          'bodytext' => 
          array (
            'config' => 
            array (
              'richtextConfiguration' => 'default',
              'enableRichtext' => 1,
            ),
          ),
        ),
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,tx_base_slider_slides,tx_base_slider_controls,tx_base_slider_indicators,tx_base_slider_use_fade,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;Animationen;animation,--palette--;Bootstrap Spacing;bootstrapSpacing,--palette--;Bootstrap Utilities;bootstrap,--palette--;Bootstrap Color;bootstrapColor,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,tx_gridelements_container,tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
      ),
    );
    $GLOBALS['TCA']['tt_content']['types'] += $tempTypes;
    

    $tempColumns = array (
      'tx_base_quote_author' => 
      array (
        'config' => 
        array (
          'type' => 'text',
          'eval' => 'required',
          'richtextConfiguration' => 'default',
          'enableRichtext' => '1',
        ),
        'exclude' => '1',
        'label' => 'LLL:EXT:base/Resources/Private/Language/locallang_db.xlf:tt_content.tx_base_quote_author',
      ),
      'tx_base_quote_picture' => 
      array (
        'config' => 
        array (
          'type' => 'inline',
          'foreign_table' => 'sys_file_reference',
          'foreign_field' => 'uid_foreign',
          'foreign_sortby' => 'sorting_foreign',
          'foreign_table_field' => 'tablenames',
          'foreign_match_fields' => 
          array (
            'fieldname' => 'tx_base_quote_picture',
          ),
          'foreign_label' => 'uid_local',
          'foreign_selector' => 'uid_local',
          'overrideChildTca' => 
          array (
            'columns' => 
            array (
              'uid_local' => 
              array (
                'config' => 
                array (
                  'appearance' => 
                  array (
                    'elementBrowserType' => 'file',
                    'elementBrowserAllowed' => 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg',
                  ),
                ),
              ),
            ),
            'types' => 
            array (
              0 => 
              array (
                'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
              ),
              1 => 
              array (
                'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
              ),
              2 => 
              array (
                'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
              ),
              3 => 
              array (
                'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
              ),
              4 => 
              array (
                'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
              ),
              5 => 
              array (
                'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
              ),
            ),
          ),
          'filter' => 
          array (
            0 => 
            array (
              'userFunc' => 'TYPO3\\CMS\\Core\\Resource\\Filter\\FileExtensionFilter->filterInlineChildren',
            ),
          ),
          'appearance' => 
          array (
            'headerThumbnail' => 
            array (
              'field' => 'uid_local',
              'width' => '45',
              'height' => '45c',
            ),
            'enabledControls' => 
            array (
              'info' => 'tx_base_quote_picture',
              'dragdrop' => 'tx_base_quote_picture',
              'hide' => 'tx_base_quote_picture',
              'delete' => 'tx_base_quote_picture',
            ),
            'fileUploadAllowed' => 'false',
          ),
          'behaviour' => 
          array (
            'localizeChildrenAtParentLocalization' => 'tx_base_quote_picture',
          ),
          'maxitems' => '1',
        ),
        'exclude' => '1',
        'l10n_mode' => 'copy',
        'label' => 'LLL:EXT:base/Resources/Private/Language/locallang_db.xlf:tt_content.tx_base_quote_picture',
      ),
      'tx_base_quote_text' => 
      array (
        'config' => 
        array (
          'type' => 'text',
          'eval' => 'required',
          'richtextConfiguration' => 'default',
          'enableRichtext' => '1',
        ),
        'exclude' => '1',
        'label' => 'LLL:EXT:base/Resources/Private/Language/locallang_db.xlf:tt_content.tx_base_quote_text',
      ),
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array(
        'LLL:EXT:base/Resources/Private/Language/locallang_db.xlf:tt_content.CType.div._base_',
        '--div--',
    );
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array(
        'LLL:EXT:base/Resources/Private/Language/locallang_db.xlf:tt_content.CType.base_quote',
        'base_quote',
        'tx_base_quote',
    );
    $tempTypes = array (
      'base_quote' => 
      array (
        'columnsOverrides' => 
        array (
          'bodytext' => 
          array (
            'config' => 
            array (
              'richtextConfiguration' => 'default',
              'enableRichtext' => 1,
            ),
          ),
        ),
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,tx_base_quote_text,tx_base_quote_author,tx_base_quote_picture,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;Animationen;animation,--palette--;Bootstrap Spacing;bootstrapSpacing,--palette--;Bootstrap Utilities;bootstrap,--palette--;Bootstrap Color;bootstrapColor,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,tx_gridelements_container,tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
      ),
    );
    $GLOBALS['TCA']['tt_content']['types'] += $tempTypes;


    $tempColumns = array (
      'tx_base_contentfilter_category' => 
      array (
        'config' => 
        array (
          'type' => 'select',
          'renderType' => 'selectSingle',
          'items' => 
          array (
            0 => 
            array (
              0 => 'Keine',
            ),
          ),
          'foreign_table' => 'sys_category',
          'maxitems' => '1',
        ),
        'exclude' => '1',
        'label' => 'LLL:EXT:base/Resources/Private/Language/locallang_db.xlf:tt_content.tx_base_contentfilter_category',
      ),
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array(
        'LLL:EXT:base/Resources/Private/Language/locallang_db.xlf:tt_content.CType.div._base_',
        '--div--',
    );
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array(
        'LLL:EXT:base/Resources/Private/Language/locallang_db.xlf:tt_content.CType.base_contentfilter',
        'base_contentfilter',
        'tx_base_contentfilter',
    );
    $tempTypes = array (
      'base_contentfilter' => 
      array (
        'columnsOverrides' => 
        array (
          'bodytext' => 
          array (
            'config' => 
            array (
              'richtextConfiguration' => 'default',
              'enableRichtext' => 1,
            ),
          ),
        ),
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,tx_base_contentfilter_category,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;Animationen;animation,--palette--;Bootstrap Spacing;bootstrapSpacing,--palette--;Bootstrap Utilities;bootstrap,--palette--;Bootstrap Color;bootstrapColor,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,tx_gridelements_container,tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
      ),
    );
    $GLOBALS['TCA']['tt_content']['types'] += $tempTypes;

