<?php
defined('TYPO3_MODE') or die();

$tempPagesColumns = [
    'tx_t3sbootstrap_container' => [
        'label' => 'Container (for the whole page)',
        'exclude' => 1,
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['no container',0],
                ['container','container'],
                ['container-fluid','container-fluid']
            ],
            'default' => 0
        ]
    ]
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages',$tempPagesColumns);
unset($tempPagesColumns);