<?php

$GLOBALS['TCA']['tx_t3sbootstrap_domain_model_config']['columns']['navbar_color']['config']['items'] = [
	['default', ''],
	['bg-light', 'light'],
	['bg-dark', 'dark'],
	['bg-primary', 'primary'],
	['bg-secondary', 'secondary'],
	['bg-success', 'success'],
	['bg-danger', 'danger'],
	['bg-warning', 'warning'],
	['bg-info', 'info'],
	['bg-white', 'white'],
	['bg-color', 'color'],
];
