# include all the backend layouts
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:base/Resources/Private/Page/Templates" extensions="ts">
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:base/Resources/Private/Grid/Templates" extensions="ts">

# Remove default backend layout from selectors
TCEFORM.pages.backend_layout_next_level.removeItems = -1
TCEFORM.pages.backend_layout.removeItems = -1

# Disable unused fields
TCEFORM.tt_content {
	date.disabled = 1
	layout.disabled = 1
	frame_class.disabled = 1
	sectionIndex.disabled = 1
	linkToTop.disabled = 1
	imageborder.disabled = 1
	imageheight.disabled = 1
	cols.disabled = 1
	table_class.disabled = 1
	table_header_position.disabled = 1
	table_tfoot.disabled = 1
	target.disabled = 1
	uploads_description.disabled = 1
	tx_t3sbootstrap_header_display.disabled = 1
	header_layout {
		label = Überschrift-Typ
		removeItems = 0, 100
		altLabels {
			1 = Überschrift 1
			2 = Überschrift 2
			3 = Überschrift 3
			4 = Überschrift 4
			5 = Überschrift 5
		}
		addItems.6 = Überschrift 6
	}
	space_before_class {
		label = Abstand davor
		keepItems = 
		addItems {
			default = Standard
			extra-small = Keiner
			small = Klein
			medium = Mittel
			large = Groß
			extra-large = Sehr groß
		}
	}
	space_after_class {
		label = Abstand danach
		keepItems = 
		addItems {
			default = Standard
			extra-small = Keiner
			small = Klein
			medium = Mittel
			large = Groß
			extra-large = Sehr groß
		}
	}
	imagewidth.label = Maximale Breite der einzelnen Bilder (px)
	table_caption.label = Tabellen-Titel
	imageorient.removeItems = 8, 9, 10, 17, 18, 25, 26
	CType {
		removeItems = textpic, bullets
	}
}

# Disable container select if page has a container already
[page|tx_t3sbootstrap_container = container]
	TCEFORM.tt_content.tx_t3sbootstrap_container.disabled = 1
[global]

mod.wizards.newContentElement.wizardItems.common.elements.image.title = Bilder
mod.wizards.newContentElement.wizardItems.common.elements.image.description = Eine beliebige Anzahl von Bildern mit Beschriftung, optional in einer Gallerie.
mod.wizards.newContentElement.wizardItems.common.elements.textmedia.title = Medien
mod.wizards.newContentElement.wizardItems.common.elements.textmedia.description = Eine beliebige Anzahl von Medien mit Beschriftung.

mod.wizards.newContentElement.wizardItems.common {
    elements {
            slider {
                iconIdentifier = tx_base_slider
                title = LLL:EXT:base/Resources/Private/Language/locallang_db_new_content_el.xlf:wizards.newContentElement.slider_title
                description = LLL:EXT:base/Resources/Private/Language/locallang_db_new_content_el.xlf:wizards.newContentElement.slider_description
                tt_content_defValues {
                    CType = base_slider
                }
            }
            quote {
                iconIdentifier = tx_base_quote
                title = LLL:EXT:base/Resources/Private/Language/locallang_db_new_content_el.xlf:wizards.newContentElement.quote_title
                description = LLL:EXT:base/Resources/Private/Language/locallang_db_new_content_el.xlf:wizards.newContentElement.quote_description
                tt_content_defValues {
                    CType = base_quote
                }
            }
            contentfilter {
                iconIdentifier = tx_base_contentfilter
                title = LLL:EXT:base/Resources/Private/Language/locallang_db_new_content_el.xlf:wizards.newContentElement.contentfilter_title
                description = LLL:EXT:base/Resources/Private/Language/locallang_db_new_content_el.xlf:wizards.newContentElement.contentfilter_description
                tt_content_defValues {
                    CType = base_contentfilter
                }
            }
    }
    show := addToList(slider, quote, contentfilter)
}


// Remove unused t3sbootstrap content elements
mod.wizards.newContentElement.wizardItems.t3sbs.elements.t3sbscarousel >
mod.wizards.newContentElement.wizardItems.t3sbs.elements.t3sbsgallery >
mod.wizards.newContentElement.wizardItems.t3sbs.elements.t3sbsmediaobject >
#tx_gridelements.setup.card_wrapper >
#tx_gridelements.setup.button_group >
tx_gridelements.setup.carousel_container >

RTE.default.preset = base

tx_gridelements.overruleRecords = 1
