<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/Static', 'Base');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_base_slider_slides');

$GLOBALS['TBE_STYLES']['skins']['base'] = [
    'name' => 'base',
    'stylesheetDirectories' => [
        'structure' => 'EXT:base/Resources/Public/Css/Backend/'
    ]
];