<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TSConfig/Page.ts">');

$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend'] = [
	'loginLogo' => 'EXT:base/Resources/Public/Images/inoovumLogo.png',
	'loginHighlightColor' => '#ef9939',
	'loginBackgroundImage' => 'EXT:base/Resources/Public/Images/inoovumBackground.jpg',
	'backendLogo' => 'EXT:base/ext_icon.svg'
];

$GLOBALS['TYPO3_CONF_VARS']['BE']['lockSSL'] = 1;

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['lang']['availableLanguages'] = ['de'];

$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['base'] = 'EXT:base/Configuration/RTE/Default.yaml';

if (TYPO3_MODE === 'BE') {
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = \SteinbauerIT\Base\Command\BaseCommandController::class;
}

$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'tx_base_slider',
    \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
    [
        'name' => 'arrows-h',
    ]
);
$iconRegistry->registerIcon(
    'tx_base_quote',
    \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
    [
        'name' => 'quote-right',
    ]
);
$iconRegistry->registerIcon(
    'tx_base_contentfilter',
    \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
    [
        'name' => 'filter',
    ]
);

// Add backend preview hook
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['base'] = 
    SteinbauerIT\Base\Hooks\PageLayoutViewDrawItem::class;
