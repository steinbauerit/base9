var iOS = false, p = navigator.platform;
if( p === 'iPad' || p === 'iPhone' || p === 'iPod' ){
	iOS = true;
}
// Cookie consent
window.addEventListener("load", function(){
	window.cookieconsent.initialise({
		"palette": {
			"popup": {
				"background": "#000"
			},
			"button": {
				"background": "#ffffff"
			}
		},
		"content": {
			"message": "Cookies erleichtern die Bereitstellung unserer Dienste. Mit der Nutzung unserer Dienste erklären Sie sich damit einverstanden, dass wir Cookies verwenden.",
			"dismiss": "Akzeptieren",
			"link": "Datenschutzerklärung",
			"href": "/datenschutz"
		}
	});
});

function baseresize() {
	winWidth = jQuery(window).width();

	$('.tailcontainer').each(function(){

			offset = $(this).offset();
			if(offset.left>=winWidth/2) {
				$(this).css('float','left');
				thisDiv = $(this).parent().parent().prev();
				if(winWidth>992) {
					imgSrc = $(thisDiv).find('img').attr('src');
					parentHeight = $(this).height();
					$(thisDiv).find('img').hide();
					$(thisDiv).css('background', 'url(' + imgSrc + ') no-repeat center center');
					$(thisDiv).css('background-size', 'cover');
					$(thisDiv).css('height',parentHeight+'px');
				} else {
					$(thisDiv).find('img').show();
					$(thisDiv).removeAttr('style');
				}
			} else {
				$(this).css('float','right');
				thisDiv = $(this).parent().parent().next();
				if(winWidth>992) {
					imgSrc = $(thisDiv).find('img').attr('src');
					parentHeight = $(this).height();
					$(thisDiv).find('img').hide();
					$(thisDiv).css('background', 'url(' + imgSrc + ') no-repeat center center');
					$(thisDiv).css('background-size', 'cover');
					$(thisDiv).css('height',parentHeight+'px');
				} else {
					$(thisDiv).find('img').show();
					$(thisDiv).removeAttr('style');
				}
			}


	});
}

// Lighcase lightbox
jQuery(document).ready(function($) {
	$('a[data-rel^=lightcase]').lightcase({
		maxWidth: 2000,
		maxHeight: 2000,
		swipe: true,
		labels: {
			'sequenceInfo.of': ' / '
		}
	});

	function onScrollInit( items, trigger ) {
        items.each( function() {
        	var osElement = $(this),
            osAnimationClass = osElement.attr('data-os-animation'),
            osAnimationDelay = osElement.attr('data-os-animation-delay');
          
            osElement.css({
                '-webkit-animation-delay':  osAnimationDelay,
                '-moz-animation-delay':     osAnimationDelay,
                'animation-delay':          osAnimationDelay
            });
            var osTrigger = ( trigger ) ? trigger : osElement;
            
            osTrigger.waypoint(function() {
                osElement.addClass('animated').addClass(osAnimationClass);
                },{
                    triggerOnce: true,
                    offset: '90%'
            });
        });
    }
    onScrollInit( $('.os-animation') );

    var mixer = mixitup('.main-content', {
    	selectors: {
			control: '.content-filter-control'
		},
	    classNames: {
	        block: '',
	        elementFilter: ''
	    }
	});

	baseresize();

});

esc=0;
$(document).keyup(function(e) {
	if (e.key === "Escape") {
		esc = esc+1;
		if(esc==3) {
			window.location.href = "/typo3";
		}
	}
});

if (iOS == true) {
} else {
	jQuery(window).resize(function() {
		baseresize();
	});
}