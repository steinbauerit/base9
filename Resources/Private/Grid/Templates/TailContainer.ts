tx_gridelements.setup.tailcontainer {
  title = Inhaltscontainer-Wrapper
  description = Passt den Inhalt einer Spalte mit voller Breite an die Inhaltsbreite an
  #icon = EXT:example_extension/Resources/Public/Images/BackendLayouts/default.gif
  config {
    colCount = 2
    rowCount = 1
    rows {
      1 {
        columns {
          1 {
            name = Inhaltscontainer
            colPos = 0
            allowed = *
          }
        }
      }
    }
  }
}