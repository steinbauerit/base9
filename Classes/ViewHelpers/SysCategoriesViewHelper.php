<?php
namespace SteinbauerIT\Base\ViewHelpers;
 

 
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
 
/*
 * <html xmlns:f="http://typo3.org/ns/TYPO3/CMS/Fluid/ViewHelpers" xmlns:id="http://typo3.org/ns/ID/Simpletemplates/ViewHelpers" data-namespace-typo3-fluid="true">
 *
 *
 *
 * // get everything with parent = 1 (array would work too)
 * <sit:SysCategory uid="1" byParentId="true" />
 * <f:debug>{sysCategoryDetailArray}</f:debug>
 *
 * // by single ID
 * <id:SysCategory uid="1" />
 * <f:debug>{sysCategoryDetailArray}</f:debug>
 *
 * // by specific IDs
 * <id:SysCategory uid="{0: 1, 1: 2, 2:3}" />
 * <f:debug>{sysCategoryDetailArray}</f:debug>
 *
 *
 * // everything:
 * <id:SysCategory />
 * <f:debug>{sysCategoryDetailArray}</f:debug>
 *
 */
 
class SysCategoriesViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
 
    /**
     * 
     */
    public function initializeArguments() {
        $this->registerArgument('parentId', 'integer', 'Parent category id', false);
    }

    /**
    *
    */
    public function render() {
        if (isset($GLOBALS['TSFE']->sys_language_uid)) {
            $language = $GLOBALS['TSFE']->sys_language_uid;
        } else {
            $language = 0;
        }
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_category');
        $query = $queryBuilder
            ->select('*')
            ->from('sys_category')
            ->where(
                $queryBuilder->expr()->eq('sys_language_uid', $language)
            );
         
        $parentId = $this->arguments['parentId'] ? $this->arguments['parentId'] : null;

        if ($parentId !== null) {
            $query->andWhere(
                $queryBuilder->expr()->in(
                    'parent',
                    $parentId
                )
            );
        }
        
        $result = $query->execute();
        $res = [];
        while ($row = $result->fetch()) {
            $res[] = $row;
        }
         
        $this->templateVariableContainer->add('categories', $res);
    }
 
}